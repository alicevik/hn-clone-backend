# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# Register your models here.
from django.contrib import admin

from models import Submission, Upvote
from models import Comment

admin.site.register(Submission)
admin.site.register(Comment)
admin.site.register(Upvote)
