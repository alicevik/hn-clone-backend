# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class Submission(models.Model):
    title = models.CharField(max_length=200)
    url = models.URLField(max_length=100)

    @property
    def upvotes(self):
        return Upvote.objects.filter(submission=self).count()

    @property
    def comments(self):
        return Comment.objects.filter(submission=self)

    def __str__(self):
        return self.title


class Upvote(models.Model):
    submission = models.ForeignKey(Submission)

    def __str__(self):
        return str(self.id)


class Comment(models.Model):
    submission = models.ForeignKey(Submission)
    content = models.TextField()

    def __str__(self):
        return self.content
