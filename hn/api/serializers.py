from rest_framework import serializers

from models import Submission, Comment, Upvote


class SubmissionSerializer(serializers.ModelSerializer):

    comment_count = serializers.SerializerMethodField()


    class Meta:
        model = Submission
        fields = ('id', 'title', 'url', 'upvotes', 'comment_count')

    def get_comment_count(self, obj):
        return len(obj.comments)


class CommentSerializer(serializers.ModelSerializer):

    submission = serializers.PrimaryKeyRelatedField(queryset=Submission.objects.all())
    class Meta:
        model = Comment
        fields = ('id', 'submission', 'content')



class InlineCommentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Comment
        fields = ('submission', 'content')

class DetailedSubmissionSerializer(serializers.ModelSerializer):

    comments = InlineCommentSerializer(many=True)
    comment_count = serializers.SerializerMethodField()

    class Meta:
        model = Submission
        fields = ('id', 'url', 'title', 'comments', 'upvotes', 'comment_count')


    def get_comment_count(self, obj):
        return len(obj.comments)



class UpvoteSerializer(serializers.ModelSerializer):

    submission = serializers.PrimaryKeyRelatedField(queryset=Submission.objects.all())

    class Meta:
        model = Upvote
        fields = ('submission', )