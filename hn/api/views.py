# -*- coding: utf-8 -*-

from rest_framework.generics import ListCreateAPIView, RetrieveAPIView
from rest_framework.response import Response

from serializers import SubmissionSerializer, DetailedSubmissionSerializer, CommentSerializer, UpvoteSerializer
from models import Submission, Comment, Upvote


class SubmissionList(ListCreateAPIView):
    queryset = Submission.objects.all()
    serializer_class = SubmissionSerializer
    pagination_class = None

    def __compare(self, item1, item2):
        if item1.upvotes < item2.upvotes:
            return -1
        elif item1.upvotes > item2.upvotes:
            return 1
        else:
            return 0

    def list(self, request, *args, **kwargs):

        queryset = self.get_queryset()

        result = sorted(queryset, cmp=self.__compare, reverse=True)
        serializer = SubmissionSerializer(result, many=True)

        data = serializer.data

        for i in range(0, len(data)):
            data[i]['index'] = i + 1

        return Response(data)


class SubmissionDetail(RetrieveAPIView):
    queryset = Submission.objects.all()
    serializer_class = DetailedSubmissionSerializer


class CommentList(ListCreateAPIView):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    pagination_class = None


class UpvoteList(ListCreateAPIView):
    queryset = Upvote.objects.all()
    serializer_class = UpvoteSerializer
    pagination_class = None
