"""hn_clone URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.contrib import admin


from django.conf.urls import url, include
from rest_framework import routers

from api import views


#router.register(r'submissions', views.SubmissionListView)


# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    url(r'^admin/', admin.site.urls),

    url(r'^api/v1/submissions/$', views.SubmissionList.as_view()),
    url(r'^api/v1/submissions/(?P<pk>[0-9]+)/$', views.SubmissionDetail.as_view()),

    url(r'^api/v1/upvotes/$', views.UpvoteList.as_view()),

    url(r'^api/v1/comments/$', views.CommentList.as_view()),

    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]

