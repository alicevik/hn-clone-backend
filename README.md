# Simple Backend for HN Clone
This project provides REST services for HN Clone. Uses sqlite for data storage.

## What You'll Need
- Python 2.7
- Django
- Django Rest Framework

## Installation
Clone the repository. You'll need python 2.7, pip and virtualenv.

Create a virtualenv wherever your project is:

```virtualenv .```

```source bin/activate```

Install the requirements by typing the following command on terminal

```pip install -r requirements.txt```

Go to hn folder and run django:

```./manage.py runserver```



